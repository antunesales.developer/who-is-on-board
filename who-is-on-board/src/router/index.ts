import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import DetailPage from '../views/DetailPage.vue'
import About from '../views/About.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  // HOME COMPONENT
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // ABOUT COMPONENT
  {
    path: '/about',
    name: 'About',
    component: About
  },
  // MONSTER DETAIL COMPONENT
  {
    path: '/:index',
    name: 'DetailPage',
    component: DetailPage
  }
]

const router = new VueRouter({
  routes
})

export default router
