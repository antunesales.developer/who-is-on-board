import axios from "axios";

export default axios.create({
  baseURL: 'https://www.dnd5eapi.co',
  headers: {
    "Content-type": "application/json; charset=utf-8"
  }
});