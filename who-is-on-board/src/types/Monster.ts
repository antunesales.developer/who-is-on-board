export default interface Monster {
  index: string;
  challenge_rating: string | number;
  type: string;
  id: null;
  name: string;
  armor_class: number;
  hit_points: number;
  hit_dice: string;
  size: string; 
  strength: number;
  dexterity: number;
  intelligence: number;
  wisdom: number;
  charisma: number;
  alignment: string;
  languages: string;
}