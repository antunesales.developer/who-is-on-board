import api from "../common-api";

const STATIC_ENDPOINT = '/api/monsters'

class MonstersListService {
  // GET ALL MONSTERS FILTERED BY THE CHALLENGE RATE
  getAll(): Promise<any> {
    return api.get(`${STATIC_ENDPOINT}?challenge_rating=3`);
  }
  // GET MONSTERS DATA BY INDEX
  get(index: any): Promise<any> {
    return api.get(`${STATIC_ENDPOINT}/${index}`);
  }
  // RETURN MONSTER SELECTED FROM LIST
  monsterSelected(index: any): Promise<any> {
    return api.get(`${STATIC_ENDPOINT}/${index}`);
  }

}

export default new MonstersListService()